import 'package:flutter/material.dart';

Column _buildButtonColumn(Color color, IconData icon, String label) {
  return Column(
    mainAxisSize: MainAxisSize.min,
    children: [
      Icon(
        icon,
        color: color,
      ),
      Container(
        child: Text(
          label,
          style: TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.w400,
            color: color,
          ),
        ),
      ),
    ],
  );
}

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;

    Widget personalInformation = Container(
      padding: const EdgeInsets.all(32),
      child: Column(
        children: [
          Container(
              child: Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [Container()],
                ),
              ),
              Icon(
                Icons.favorite,
                color: Colors.red[600],
              ),
            ],
          )),
          Container(
              child: Text(
            'ข้อมูลส่วนตัว',
            style: TextStyle(fontWeight: FontWeight.bold),
          )),
          Container(
              child: Column(children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text("ชื่อ-สกุล: กิตติมศักดิ์ ศรีอุดม"),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text("ชื่อเล่น: ทิว"),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text("เกิดวัน 18 พฤษภาคม 2542"),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text("อายุ 22 ปี"),
              ),
            )
          ])),
          Container(
              child: Text(
            'การศึกษา',
            style: TextStyle(fontWeight: FontWeight.bold),
          )),
          Container(
              child: Column(children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text(
                  "ปัจจุบัน",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text("สถานศึกษา: มหาวิทยาลัยบูรพา"),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text("คณะ: วิทยาการสารสนเทศ"),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text("สาขา: วิทยาการคอมพิวเตอร์"),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text(
                  "มัธยมปลาย",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text("สถานศึกษา: โรงเรียนกัลยาณวัตร"),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text("หลักสูตร: วิทย์ - คณิต"),
              ),
            ),
          ])),
          Container(
              padding: const EdgeInsets.all(32),
              child: Text(
                'งานอดิเรก',
                style: TextStyle(fontWeight: FontWeight.bold),
              )),
          Container(
              child: Column(children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text("- ชอบฟังเพลง"),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text("- เล่นวอลเลย์บอล"),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text("- เขียนโค้ด"),
              ),
            ),
          ])),
        ],
      ),
    );
    Widget buttonEducation = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(color, Icons.call, 'CALL'),
          _buildButtonColumn(color, Icons.email, 'EMAIL'),
          _buildButtonColumn(color, Icons.facebook, 'FACEBOOK'),
        ],
      ),
    );
    return MaterialApp(
        title: 'Resume',
        home: Scaffold(
          appBar: AppBar(
            title: const Text('RESUME'),
          ),
          body: ListView(children: [
            Image.asset('images/teew2.jpg',
                width: 600, height: 500, fit: BoxFit.cover),
            personalInformation,
            buttonEducation,
          ]),
        ));
  }
}
